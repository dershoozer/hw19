﻿#include <iostream>

using namespace::std;

    
class Animal
{

public:

    virtual void Voice() const = 0;
};

class Dog:public Animal
{

public:
   
    void Voice() const override
    {
        cout << "Woof!\n";
    }
};

class Cat :public Animal
{

public:

    void Voice() const override
    {
        cout << "Meow!\n";
    }
};

class Cow :public Animal
{

public:

    void Voice() const override
    {
        cout << "Moo!\n";
    }
};

class Lion :public Animal
{

public:

    void Voice() const override
    {
        cout << "Roar!\n";
    }
};

class Mouse :public Animal
{

public:

    void Voice() const override
    {
        cout << "Squeak!\n";
    }
};

int main()
{

    Animal* animals[5]{};
    
    animals[0] = new Dog();
    animals[1] = new Cat();
    animals[2] = new Cow();
    animals[3] = new Lion();
    animals[4] = new Mouse();
    
    for (Animal* a : animals) a->Voice();

    //delete[]animals
    //При попытке освободить память выдает ошибку. Почему?
};

